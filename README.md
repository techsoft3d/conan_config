

## Using this repo
On a machine which uses Conan to build or package a product, this must be run first:
`conan config install https://bitbucket.org/techsoft3d/conan_config.git`

This will install the configuration for repositories and profiles. For more information see:
https://docs.conan.io/en/latest/reference/commands/consumer/config.html#conan-config-install